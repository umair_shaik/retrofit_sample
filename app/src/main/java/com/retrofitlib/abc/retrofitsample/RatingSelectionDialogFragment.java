package com.retrofitlib.abc.retrofitsample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by umshaik on 11/10/16.
 */
public class RatingSelectionDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        CharSequence[] mSelectedItems = this.getResources().getStringArray(R.array.pref_list_rating_range);  // Where we track the selected items
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        builder.setTitle(R.string.title_checkbox_preference)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(mSelectedItems, 0,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getActivity(),"In Cusine "+i,Toast.LENGTH_SHORT).show();
                                dismiss();
                            }
                        });


        return builder.create();
    }
}
