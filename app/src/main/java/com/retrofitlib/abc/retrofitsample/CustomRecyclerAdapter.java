package com.retrofitlib.abc.retrofitsample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.retrofitlib.abc.retrofitsample.pojo.NearbyRestaurant;

import java.util.List;

/**
 * Created by umshaik on 11/3/16.
 */

public class CustomRecyclerAdapter extends RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder> {
    private final List<NearbyRestaurant> dataSet;

    public CustomRecyclerAdapter(List<NearbyRestaurant> restaurants) {
        this.dataSet = restaurants;
    }

    @Override
    public CustomRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomRecyclerAdapter.ViewHolder holder, int position) {
        holder.getTextView().setText(dataSet.get(position).getRestaurant().getName());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text_list_view);
        }

        public TextView getTextView() {
            return textView;
        }
    }
}
