package com.retrofitlib.abc.retrofitsample;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.retrofitlib.abc.retrofitsample.pojo.NearbyRestaurant;
import com.retrofitlib.abc.retrofitsample.pojo.RestaurantsResponce;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 1001;
    private double lat;
    private double lng;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog loading;
    private List<NearbyRestaurant> restaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buildGoogleApiClient();
        getRestaurants();

    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    @Override
    protected void onStart() {
        super.onStart();
        loading = ProgressDialog.show(this, "Fetching Data", "Please wait...", false, false);
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)) {


                } else {


                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);


                }
            }
        } else {
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(10); // Update location every second

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            lat = location.getLatitude();
            lng = location.getLongitude();
            mGoogleApiClient.disconnect();
            getRestaurants();
        } else {
            Toast.makeText(this, this.getString(R.string.location_fetch_failed), Toast.LENGTH_LONG).show();
        }
    }


    private void getRestaurants() {
        lat = 19.145999;
        lng = 72.990953;

        if (!checkIfLatLongValuesAreSame()) {

            //populateRecyclerView(new ArrayList<NearbyRestaurant>());

            // Create a very simple REST adapter which points the GitHub API endpoint.
            RestaurantAPI client = ServiceGenerator
                    .getClient()
                    .create(RestaurantAPI.class);


            // Fetch and print a list of the contributors to this library.
            //Call<RestaurantsResponce> call = client.getTask(19.145999, 72.990953);
            Call<RestaurantsResponce> call = client.getTask(lat, lng);
            call.enqueue(new Callback<RestaurantsResponce>() {
                @Override
                public void onResponse(Call<RestaurantsResponce> call, Response<RestaurantsResponce> response) {
                    okhttp3.Response raw = response.raw();
                    if (response.isSuccessful()) {

                        //Log.d("success", response.toString() + "\n" + raw.toString());
                        restaurants = response.body().getNearbyRestaurants();
                        //Log.d("total", String.valueOf(restaurants.size()) + "\n" + restaurants.get(1).getRestaurant().getName());

                        populateRecyclerView(restaurants);

                    } else if (restaurants != null) {
                        Log.d("No Response", response.toString() + "\n" + raw.toString());
                        populateRecyclerView(restaurants);
                    }
                }

                @Override
                public void onFailure(Call<RestaurantsResponce> call, Throwable t) {
                    Log.d("Error", t.getMessage() + t.toString());

                }

            });
        } else {
            //TODO pick the result from previous network call
            //TODO Still working on it
            RestaurantAPI client = ServiceGenerator
                    .getClient()
                    .create(RestaurantAPI.class);


            // Fetch and print a list of the contributors to this library.
            //Call<RestaurantsResponce> call = client.getTask(19.145999, 72.990953);
            Call<RestaurantsResponce> call = client.getTask(lat, lng);
            call.enqueue(new Callback<RestaurantsResponce>() {
                @Override
                public void onResponse(Call<RestaurantsResponce> call, Response<RestaurantsResponce> response) {
                    okhttp3.Response raw = response.raw();
                    if (response.isSuccessful()) {

                        //Log.d("success", response.toString() + "\n" + raw.toString());
                        restaurants = response.body().getNearbyRestaurants();
                        //Log.d("total", String.valueOf(restaurants.size()) + "\n" + restaurants.get(1).getRestaurant().getName());

                        populateRecyclerView(restaurants);

                    } else {
                        Log.d("No Response", response.toString() + "\n" + raw.toString());

                    }
                }

                @Override
                public void onFailure(Call<RestaurantsResponce> call, Throwable t) {
                    Log.d("Error", t.getMessage() + t.toString());

                }

            });

        }

    }

    private boolean checkIfLatLongValuesAreSame() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String localLatitude = preferences.getString(this.getString(R.string.latitude), null);
        String localLongitude = preferences.getString(this.getString(R.string.longitude), null);

        if (localLatitude == null && null == localLongitude) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(MainActivity.this.getString(R.string.latitude), String.valueOf(lat));
            editor.putString(MainActivity.this.getString(R.string.longitude), String.valueOf(lng));
            editor.apply();
            return false;
        } else {
            //String tempLat = new DecimalFormat("#.##").format(Double.parseDouble(localLatitude));
            //String tempLng = new DecimalFormat("#.##").format(Double.parseDouble(localLongitude));
            double tmpLat = Math.floor(Double.parseDouble(localLatitude) * 100) / 100;
            double tmpLng = Math.floor(Double.parseDouble(localLongitude) * 100) / 100;

            if ((Math.floor(lat * 100) / 100) == tmpLat && (Math.floor(lng * 100) / 100) == tmpLng) {
                return true;
            } else return false;
        }
    }

    private void populateRecyclerView(List<NearbyRestaurant> restaurants) {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.listViewRestaurant);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        restaurants = getFilterValue(restaurants);
        CustomRecyclerAdapter mAdapter = new CustomRecyclerAdapter(restaurants);

        mRecyclerView.setAdapter(mAdapter);
        loading.dismiss();
    }

    private List<NearbyRestaurant> getFilterValue(List<NearbyRestaurant> restaurants) {

        //lets say user-rating >3.2
        List<NearbyRestaurant> filteredRestaurant = new ArrayList<NearbyRestaurant>();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String ratingValue = preferences.getString(this.getString(R.string.rating_value), null);
        Set<String> cusinesTypes = preferences.getStringSet(this.getString(R.string.cuisin_types), null);

        if (ratingValue != null && cusinesTypes.size() != 0) {

            for (NearbyRestaurant o : restaurants) {

                if (Double.parseDouble(o.getRestaurant().getUserRating().getAggregateRating()) >
                        Double.parseDouble(ratingValue)) {
                    filteredRestaurant.add(o);
                }
            }


            String[] cusinesTypesArray = cusinesTypes.toArray(new String[]{});
            for (NearbyRestaurant o : restaurants) {

                if (o.getRestaurant().getCuisines().contains(cusinesTypesArray[0])) {
                    filteredRestaurant.add(o);
                }
            }

            return filteredRestaurant;
        } else return restaurants;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }*/

        switch (id){
            case R.id.rating_item:
                Toast.makeText(this,"In rating",Toast.LENGTH_SHORT).show();
                DialogFragment newRatingFragment = new RatingSelectionDialogFragment();
                newRatingFragment.show(getSupportFragmentManager(), "ratings");
                break;
            case R.id.cuisines_item:
                Toast.makeText(this,"In Cusine",Toast.LENGTH_SHORT).show();
                DialogFragment newCuisineFragment = new CuisineSelectionDialogFragment();
                newCuisineFragment.show(getSupportFragmentManager(), "ratings");

                break;
            default:Toast.makeText(this, R.string.select_vaild_option,Toast.LENGTH_SHORT).show();

        }

        return super.onOptionsItemSelected(item);
    }

}
