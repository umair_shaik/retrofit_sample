package com.retrofitlib.abc.retrofitsample.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umshaik on 11/3/16.
 */

public class RestaurantsResponce {
    @SerializedName("nearby_restaurants")
    private List<NearbyRestaurant> nearbyRestaurants = new ArrayList<NearbyRestaurant>();

    @SerializedName("popularity")
    @Expose
    private Popularity popularity;
    @SerializedName("link")
    @Expose
    private String link;

    public Popularity getPopularity() {
        return popularity;
    }

    public void setPopularity(Popularity popularity) {
        this.popularity = popularity;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<NearbyRestaurant> getNearbyRestaurants() {
        return nearbyRestaurants;
    }

    public void setNearbyRestaurants(List<NearbyRestaurant> nearbyRestaurants) {
        this.nearbyRestaurants = nearbyRestaurants;
    }

    @Override
    public String toString() {
        return "RestaurantsResponce{" +
                "nearbyRestaurants=" + nearbyRestaurants +
                '}';
    }
}
