package com.retrofitlib.abc.retrofitsample;

import com.retrofitlib.abc.retrofitsample.pojo.RestaurantsResponce;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by ABC on 02/11/2016.
 */

public interface RestaurantAPI {

    @Headers("user-key: 9b48f6666c779c167a90bfbb864ad66f")
    @GET("/api/v2.1/geocode")
    Call<RestaurantsResponce> getTask(@Query("lat") double lat,
                                      @Query("lon") double lon);
}
