package com.retrofitlib.abc.retrofitsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.MultiSelectListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

import java.util.Set;

/**
 * Created by umshaik on 11/4/16.
 */

public class CustomMultiSelectListPreference extends MultiSelectListPreference {

    public CustomMultiSelectListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        getSummaryValues();

    }

    private void getSummaryValues() {
        String summaryText = "";
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Set<String> selections = preferences.getStringSet(getContext().getString(R.string.cuisin_types), null);

        if (selections != null) {
            String[] selected = selections.toArray(new String[]{});

            for (int i = 0; i < selected.length; i++) {
                System.out.println("\ntest" + i + " : " + selected[i]);
                summaryText = selected[i] + "," + summaryText;
                setSummary(summaryText);
            }
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            getSummaryValues();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            Set<String> selections = preferences.getStringSet(getContext().getString(R.string.cuisin_types), null);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putStringSet(getContext().getString(R.string.cuisin_types), selections);
            editor.commit();
        }
    }
}
