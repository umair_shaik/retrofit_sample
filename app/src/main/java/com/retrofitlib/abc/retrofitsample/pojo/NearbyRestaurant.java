package com.retrofitlib.abc.retrofitsample.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by umshaik on 11/3/16.
 */
public class NearbyRestaurant {

    @SerializedName("restaurant")
    private Restaurant restaurant;

    /**
     * @return The restaurant
     */
    @SerializedName("restaurant")
    public Restaurant getRestaurant() {
        return restaurant;
    }

    /**
     * @param restaurant The restaurant
     */
    @SerializedName("restaurant")
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public String toString() {
        return "NearbyRestaurant{" +
                "restaurant=" + restaurant +
                '}';
    }
}
